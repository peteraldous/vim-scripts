" wraps lines at 70 chars
au FileType plaintex,tex set textwidth=70
" Makes sure there are no double spaces after a sentence.
" This setting won't make a difference with each sentence on its own line.
au FileType plaintex,tex set nojoinspaces

function! FormatUniqueLines()
  normal mr
  let paraend = line("'}")
  normal {))
  if line('.') >= paraend
    normal `r
    normal gwas
  else
    while line(".") < paraend
      " Go back to the punctuation that ended the last sentence
      call search("[.!?][\]')}]*", "b")
      " When the previous sentence ends at the end of a line, don't substitute
      " the newline that's already there
      if strlen(getline('.')) == col('.')
        " simply move back to the sentence
        normal )
      else
        " change all of the whitespace between the sentence's end and the next
        " sentence to a single newline
        call search("[ \t\v]")
        normal c)
      endif
      " format the current sentence
      normal gwas
      " the location of the end of the paragraph may have changed
      let paraend = line("'}")
      " go to the next sentence and iterate
      normal )
    endwhile
  endif
  unlet paraend
  normal `r
  delm r
endfunction
au FileType plaintex,tex map <C-S-f> :call FormatUniqueLines()<CR>

