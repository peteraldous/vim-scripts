There isn't much to this repository. It's just some vim scripts. I'll try to add useful comments. The point of this is to help people to spend less time getting this functionality than I had to spend.

Incidentally, Vim scripting is relatively new to me. I would love to learn more about how to write idiomatic scripts. Please don't hesitate to comment or to send a pull request.
